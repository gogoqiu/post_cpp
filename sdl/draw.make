
all: bin/draw.o
	gcc -o bin/draw bin/draw.o -lSDL2 -lSDL2_ttf  -lstdc++  -lm  -std=c++11
bin/draw.o : src/Eyas3D/Examples/draw.cpp
	gcc -c -o bin/draw.o src/Eyas3D/Examples/draw.cpp -I/usr/include/SDL2 

clean:
	rm bin/draw.o bin/draw
