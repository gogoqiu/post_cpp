
#include "IRenderHandler.h"						//IRenderHandler::Render();
#include "IKeyBoardEventHandler.h"	//IKeyBoardEventHandler::OnKeyUp, IKeyBoardEventHandler::OnKeyDown
#include "IMouseEventHandler.h"		//IMouseEventHandler::OnMouseClick;	IMouseEventHandler:: OnMouseUp;	IMouseEventHandler:: OnMouseDown

/*
 * 
 * 系统onRelease(){
 * 	释放资源
 * 	SDL_Close();
 * }
 * 从建立Game game开始，还是从SDL_init()开始
 * class ISystem{
 * }
 * class System{
 * 	void createWindow();
 * 	void initSystem(){
 * 		SDL_init();
 * 	};
 * }
 */
//eventloop实际就是融合各种io的过程
class ioFusion	//io融合
{
public:
	//然后
	//如何支持键盘按键匹配的虚拟层．按键可能代表的意思不同
	//这是程序的下半部分，事件处理．程序的上半部分，就是建立窗口
	static void eventLoop(){
		SDL_Event myEvent;//事件
		int x,y;	short delta;
		//int quit = 0;
		while ( 1 ) //建立事件主循环
		{
			if ( SDL_PollEvent ( &myEvent ) ) //从队列里取出事件
			{
				//获得事件
				switch ( myEvent.type ) //根据事件类型分门别类去处理
				{				
				case SDL_KEYUP:
					switch( myEvent.key.keysym.sym )
					{
						case SDLK_LEFT:
							break;
						case SDLK_RIGHT:
							break;
						case SDLK_UP:
							break;
						case SDLK_SPACE:
							break;
						default:
							break;
					}
					//cout<<myEvent.key.keysym.sym<<endl;
					//printf( "keyup: %d\n", myEvent.key.keysym.sym );
					EGameWindow::GWindow->onKeyRelease( myEvent.key.keysym.sym );
					break;
				case SDL_KEYDOWN:
					//printf( "keydown: %d\n", myEvent.key.keysym.sym );
					EGameWindow::GWindow->onKeyDown( myEvent.key.keysym.sym );
					break;
				case SDL_MOUSEMOTION:
					x	= ( myEvent.motion.x );
					y	= ( myEvent.motion.y );
					EGameWindow::GWindow->onMouseMove( x, y );
					break;
				case SDL_MOUSEWHEEL:
					delta = (short)myEvent.motion.y; /* wheel rotation */
					EGameWindow::GWindow->onMouseWheel( delta * 0.5f / PI );
					break;
				case SDL_MOUSEBUTTONDOWN:
					break;
				case SDL_MOUSEBUTTONUP:
					break;
				case SDL_QUIT://如果是离开事件
					mQuite = true;
					break;
				case SDL_WINDOWEVENT:
					switch (myEvent.window.event) {
					case SDL_WINDOWEVENT_SHOWN:
						SDL_Log("Window %d shown", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_HIDDEN:
						SDL_Log("Window %d hidden", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_EXPOSED:
						EGameWindow::GWindow->onPaint( 0 );
						SDL_Log("Window %d exposed", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_MOVED:
						SDL_Log("Window %d moved to %d,%d",
								myEvent.window.windowID, myEvent.window.data1,
								myEvent.window.data2);
						break;
					case SDL_WINDOWEVENT_RESIZED:
						SDL_Log("Window %d resized to %dx%d",
								myEvent.window.windowID, myEvent.window.data1,
								myEvent.window.data2);
						break;
					case SDL_WINDOWEVENT_SIZE_CHANGED:
						SDL_Log("Window %d size changed to %dx%d",
								myEvent.window.windowID, myEvent.window.data1,
								myEvent.window.data2);
						break;
					case SDL_WINDOWEVENT_MINIMIZED:
						SDL_Log("Window %d minimized", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_MAXIMIZED:
						SDL_Log("Window %d maximized", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_RESTORED:
						SDL_Log("Window %d restored", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_ENTER:
						SDL_Log("Mouse entered window %d",
								myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_LEAVE:
						SDL_Log("Mouse left window %d", myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_FOCUS_GAINED:
						SDL_Log("Window %d gained keyboard focus",
								myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_FOCUS_LOST:
						SDL_Log("Window %d lost keyboard focus",
								myEvent.window.windowID);
						break;
					case SDL_WINDOWEVENT_CLOSE:
						mQuite = true;						
						SDL_Log("Window %d closed", myEvent.window.windowID);
						break;
					default:
						SDL_Log("Window %d got unknown event %d",
								myEvent.window.windowID, myEvent.window.event);
						break;
					}
					//cout<<"SDL_WINDOWEVENT"<<endl;
					break;
				default: // Report an unhandled event, 512, 1025, 1026
					//printf("I don't know what this event is!\n");
					//cout<<myEvent.type<<endl;
					//printf( "unknown event: %d\n", myEvent.type );
					break;
				}
				onUpdate();
			}else{
				//没获得事件
				//::SDL_Delay( 100 );
			}		
			//render
			//一帧一帧渲染
			//撤掉game层
			render();	
		}
		SDL_DestroyWindow ( mHwnd );
		SDL_Quit();
	}
}
