
#include <SDL.h>
//通过接口约定，通过函数绑定
struct IKeyBoardEventHandler
{
	virtual void onKeyClick( SDL_Event * event )=0;
	virtual void onKeyUp( SDL_Event * event )=0;
	virtual void onKeyDown( SDL_Event * event )=0;
}
