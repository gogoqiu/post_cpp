
#include <SDL.h>
struct IMouseEventHandler
{
	void onMouseClick( SDL_Event * event );
	void onMouseUp( SDL_Event * event );
	void onMouseDown( SDL_Event * event );
}
