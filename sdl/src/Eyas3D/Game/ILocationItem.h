
class ILocationItem
{
public:
	//x,y,z也可以是整数．斜角移动也算１，而不是1.414等
	//Renderer通过获得位置信息，进行渲染
	//当前动画状态，关系到角色动画帧
	virtual float getX();
	virtual float getY();
	virtual float getZ();
	virtual bool Move( float x, float y, float z );
	//NPC就是只实现这个接口，而不是实现IBattleItem
	virtual void setX( float x );
	virtual void setY( float y );
	virtual void setZ( float z );
	//地图是什么？其实最简单的数据结构，如下
	/*
	class IMap{
	public:
		int getHeight():	//高永远是１
		int getWidth();
		void setWidth( int width );
		void setHeight( int height );
		void setDeep( int deep );
		int getDeep();
	}
	*/
	virtual float getSpeed();	//0.2秒的时速．单位移动量
	virtual void setSpeed( float speed );
	//附属向量．Vector．命令向量．
	//日常就是Vector(0,0,0)
	//每次移动都会修整向量
	virtual void computeCurrentLocation();
}

//半秒更新ILocationItem的状态，就是动画了
//具有路径的．分段执行的
