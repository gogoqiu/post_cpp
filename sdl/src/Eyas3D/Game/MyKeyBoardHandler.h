
#include <SDL.h>
#include "../System/IKeyBoardHandler.h"

class MyKeyBoardHandler: public IKeyBoardHandler
{
	Game *game;
	KeyBoardHandler( Game *game ){
		this->game = game;
	};
	virtual void onKeyClick( SDL_Event * event ){
		SDL_Log( "key clicked" );
	};
	virtual void onKeyUp( SDL_Event * event ){
		SDL_Log( "key up" );
	};
	virtual void onKeyDown( SDL_Event * event ){
		SDL_Log( "key down" );
	};;
	static void demo1(){
		// a eventloop with IKeyBoardHandler point
		//最基础的eventloop也可以使用接口．那么所有后面的都可以通过implement这个接口就好
	}
	static void demo2(){
		
	}
}
