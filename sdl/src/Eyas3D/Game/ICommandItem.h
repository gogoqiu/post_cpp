

//ICommandItem封装进commandItems
//鼠标点击或者键盘输入产生ICommandItem，置入序列里
//命令交换成实际效果．
//输入->ICommandItem->影响内部逻辑(副产品：显示出来)
//输入捕捉通过获得事件实现，同时封装为ICommandItem
//ICommandItem的效果在事件处理函数中处理．渲染也是在事件处理函数中
//点击输入投射到＂虚拟物件＂世界坐标

//当前对象位置，鼠标右击（选择）位置
//Vector<thislocation->nextLocation>.
//
//每个对象只有唯一当前命令．在没有更新命令时，保持当前命令的执行．命令置空
//只有移动命令
//Result计算方式．相互关系的抽象
class ICommandItem
{
public:
	IGameItem *getHost();
	void setHost( IGameItem* );
	Command::CommandType getCommandType();
	Command::CommandData getCommandData();
	void setCommandType( Command::CommandType );
	void setCommandData( Command::CommandData );
	void setEnable( bool enable );
	bool getEnable();
}