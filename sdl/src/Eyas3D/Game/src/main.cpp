#include "EGraphics.h"
#include "EWindow.h"
#include "EMeshUtil.h"
#include "EUtil.h"
#include "EGame.h"
#include "ERuntime.h"
//#include "../res/resource.h" 

using namespace Eyas3D;

int main( int argc, char* argv[] )
{
	ERuntime runtime;
	runtime.info();
	InitLog( "Eyase3D.Log" );
	Log( "Init Graphics..." );
	EGraphics *graphics = new EGraphics();
	graphics->initGraphics(  );
	Log( "Graphics Load Successed!" );
	//EGameWindow window;
	EGameWindow::GWindow = new EGameWindow( "Eyas3D [3DTankWar]", graphics );
	EGameWindow::GWindow->showWindow( true );
	/* 设置游戏的小图标 */
	SDL_Window* hwnd = EGameWindow::GWindow->getHWnd();
	//LONG	iconID	= (LONG) LoadIcon( ::GetModuleHandle( 0 ), MAKEINTRESOURCE( IDI_ICON_TANKWAR ) );
	//::SetClassLong( hwnd, GCL_HICON, iconID );
	//
	EGame *game = new EGame;
	/* game what */
	//static EGameWindow *GWindow;
	//
	EGameWindow::GWindow->setCanvasListener( game );
	/* game what */
	EGameWindow::GWindow->addInputListener( game );
	EGameWindow::GWindow->startLoop();
	Log( "Shutdown Graphics..." );
	graphics->shutdownGraphics();
	delete game;
	delete graphics;
	CloseLog();
	return 0;
}
