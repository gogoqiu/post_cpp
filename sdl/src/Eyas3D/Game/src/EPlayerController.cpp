
#include "EPlayerController.h"
#include "EGameManager.h"

namespace Eyas3D
{
	/* -------------------------------------------------------------------------- */
	EPlayerController::EPlayerController( EGameManager *gameMgr )
		: mCurDir( 0 ), mGameMgr( gameMgr ), mScene( gameMgr->getSceneManager() ), mFireDir( 0 )
	{
		mCamera = mScene->getCamera();
		mTank	= new ETank( "Player", "Green", gameMgr );
	}
	/* -------------------------------------------------------------------------- */
	EPlayerController:: ~EPlayerController()
	{
		SafeDelete( mTank );
	}
	/* -------------------------------------------------------------------------- */
	void EPlayerController::setPosition( const EVector3D &pos )
	{
		/*
		 * 由于坦克和摄像机被绑定到一起所以在修改坦克的位置时
		 * 需要修改摄像机的位置和目标点
		 */
		mTank->setPosition( pos );
		mCamera->setPosition( pos + EVector3D( 0, 25, 30 ) );
		mCamera->setTarget( pos );
	}
	/* -------------------------------------------------------------------------- */
	void EPlayerController::keyPress( EInt key )
	{
		if ( key == 119 )	//W
			mCurDir |= DIR_UP;
		else if ( key == 115 )//S
			mCurDir |= DIR_DOWN;
		else if ( key == 97 )//A
		{
			mCurDir |= DIR_LEFT;
			mTank->turnLeft();
		}else if ( key == 100 )//D
		{
			mCurDir |= DIR_RIGHT;
			mTank->turnRight();
		}else if ( key == 1073741904 )//VK_SPACE
			mTank->fire();
		else if ( key == 1073741903 )//VK_LEFT
		{
			mFireDir = -1;
			/* mTank->fireAimLeft(); */
		}else if ( key == 39 )//VK_RIGHT
		{
			mFireDir = 1;
			/* mTank->fireAimRight(); */
		}else if ( key == 102 )//F
		{
			if ( mCamera->getRenderMode() == RENDER_WIRE )
				mCamera->setRenderMode( RENDER_SOILD );
			else
				mCamera->setRenderMode( RENDER_WIRE );
		}else if ( key == 103 )//G
		{
			mTank->setBulletType( mTank->getBulletTye() == BULLET_ROCKET ? BULLET_BALL : BULLET_ROCKET );
		}
	}
	/* -------------------------------------------------------------------------- */
	void EPlayerController::keyRlease( EInt key )
	{
		if ( key == 119 )//W
			mCurDir &= ~DIR_UP;
		else if ( key == 115 )//S
			mCurDir &= ~DIR_DOWN;
		else if ( key == 97 )//A
			mCurDir &= ~DIR_LEFT;
		else if ( key == 100 )//D
			mCurDir &= ~DIR_RIGHT;
		else if ( key == 37 || key == 39 )//
			mFireDir = 0;
	}
	/* -------------------------------------------------------------------------- */
	void EPlayerController::update()
	{
		if ( mCurDir & DIR_UP )
		{
			mTank->moveUp();
			mCamera->move( mTank->getCameraStep() );
		}else if ( mCurDir & DIR_DOWN )
		{
			mTank->moveDown();
			mCamera->move( mTank->getCameraStep() );
		}
		if ( mFireDir == -1 )
			mTank->fireAimLeft();
		else if ( mFireDir == 1 )
			mTank->fireAimRight();
		mTank->update();
	}
	/* -------------------------------------------------------------------------- */
}
