
class IBattleItem
{
public:
	enum Color{
		red, blue
	}
	virtual int getAttackPower();
	virtual void setAttackPower( int attackpower );
	virtual int	getDefendPower();
	virtual void setDefendPower( int defendpower );
	//
	virtual float getAttackRadius();
	virtual void setAttackRadius( float );
	virtual void setHitPoint( int hitpoint );
	virtual int getHitPoint();
	virtual Color getColor();
	virtual void setColor( Color );
	virtual bool isDead();
	//常规打击
	//bool attack( IBattleItem * defender );
	//技能1...
	//IBattleItem();
	//IBattleItem( int hitpoint, int attackpower, int defendpower )
}