
//ICommandItem的容器以及处理ICommandItem序列的静态函数
//所谓command，其实就是task．完成任务
class Command
{
	//
public:
	enum CommandType{
		Move, 	//以地面为选择物．以移动到位置为终结
		Attack,	//以其他战斗对象选择物.attack的终结是对方死了．
		None	//没有任务．
	};
	//哪里封装和获得这个数据
	union CommandData{
		struct{
			ILocationItem* mapItem;
			Vector vector;//偏移量
		} move_command_data;
		struct{
			IBattleItem *attacker;
			IBattleItem *defender;//强制转换为父类？然后通过ILocationItem进行计算位置转移？
		} attack_command_data;
	};
	static void clearCommands(){
		ICommandItem *item;
		std::list<ICommandItem*>::iterator it1,it2;
		for( it1=commands.begin(); it1!=commands.end(); ++it1 ){
			if( !it1->getEnable() )
				commands.erase( it1 );
		}
	}
	static void disableCommands( IGameItem * item ){
		//将特定command设置为不可用
		//CommandData commanddata;
		ICommandItem *item;
		//ILocationItem* mapItem;
		for( int i=0; i<commands.size(); i++ ){
			item = commands.get(i);
			if( item->getHost()== item ){
				item.setEnable( false );
			}
		}
	}
	//commands是不是做成静态的
	std:list<ICommandItem*> commands;
	//this step parse
	//延时作用
	//animation step
	//command step
	//结算一下
	//static update
	//中间层之管理
	//location step, animation step
	//static location
	static void attack_step(){
		//循环播放
		mapItem->setCurrentAnimationGroup( IAnimationItem::AnimationGroup::attack );
		int cf = mapItem->getCurrentFrame();
		int fc = mapItem->getFrameCount();
		if(cf<fc)
			mapItem->setCurrentFrame( ++cf );
		else
			mapItem->setCurrentFrame( 1 );
	}
	static void move_step(){
		//循环播放
		mapItem->setCurrentAnimationGroup( IAnimationItem::AnimationGroup::move );
		int cf = mapItem->getCurrentFrame();
		int fc = mapItem->getFrameCount();
		if(cf<fc)
			mapItem->setCurrentFrame( ++cf );
		else
			mapItem->setCurrentFrame( 1 );
	}
	static void step(){
		for( int i=0; i<commands.size(); i++ ){
			ICommandItem *item = commands.get(i)
			//Result.attackResult
			switch( item->commandtype ){
				//寄存的任务
				//合并
			case Move:	//CommandType::Move
				//计算距离．距离大于速度．设置动画组为移动．
				//距离小于速度，动画也是移动

				//mapItem->
				//一旦达成，就删除这个这个命令
				//后面进来的command
				//不是陆续完成每个command．而是
				if( item->getEnable() ){

				}
				break;
			case Attack:
				//Result.attackResult( attack_command_data.attacker, attack_command_data.defender );
				//计算距离．距离大于速度．设置动画组为移动．
				//计算与防御者的距离．
				//在计算当时，进攻者和防御者的位置都是不变化的
				//向量取绝对值
				//
				//计算距离，距离等于小于进攻距离，设置动画组为进攻
				//
				if( item->getEnable() ){
					
				}
				break;
			case None:
				break;
			default:
				//没有命令，啥都不改变
				//输出日志
				break;
			}
		}
	}
	static void parse(){
		//面向具体对象．
		for( int i=0; i<commands.size(); i++ ){
			ICommandItem *item = commands.get(i)
			//Result.attackResult
			switch( item->commandtype ){
			case Move:	//CommandType::Move
				//mapItem->

				break;
			case Attack:
				//Result.attackResult( attack_command_data.attacker, attack_command_data.defender );
				break;
			default:
				//输出日志
				break;
			}
		}
	}

}