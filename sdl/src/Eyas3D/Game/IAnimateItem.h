

class IAnimateItem
{
public:
	//8帧动画
	//都是总数为８帧的动画
	//运动8帧．打击8帧
	//Command里的enum的attack和move与这个类里的enum的attack和move是两种概念．command里的决定命令的attack或move，这里的enum决定当前动画的attack,move
	//当没有到达被攻击者的周边，是无法变成attack动画的，还是move动画
	enum AnimationGroup{
		attack, move
	}
	virtual AnimationGroup getCurrentAnimationGroup(  );
	virtual void setCurrentAnimationGroup( AnimationGroup ag );
	virtual int getCurrentFrame();
	virtual void setCurrentFrame( int idx );
	virtual int getFrameCount();
	virtual void setFrameCount( int count );
	virtual void computeCurrentFrame();
};