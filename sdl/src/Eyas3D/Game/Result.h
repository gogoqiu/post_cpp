
//Result类和Renderer一样，涵盖一些不用修改的固定的算法，比如伤害和攻击力的关系
//终结计算
//需要哪些类，哪些节点，哪些接口
class Result
{
public:
	enum Relation{
		Enemy, Friend
	}relation;
	//一般攻击算法．可以套用这个函数．也可以通过实现IBattleItem的attack算法实现特定
	static void attackResult( IBattleItem *attack, IBattleItem* defender );
	//返回值是剩余的向量
	//如何计算剩余量，以及关于速度的计算
	static Vector4D moveStepResult( ILocationItem* item, Vector4D pos );
}