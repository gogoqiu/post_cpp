


#include <stdio.h>

//typedef std::vector<EPolyon4D>	EPolyon4DArray;
//typedef std::vector<EVector4D>	EVector4DArray;
namespace Eyas3D
{
	namespace Core
	{
		class CompEPolyonF4D
		{
			EBool operator() ( const EPolyonF4D &polyA, const EPolyonF4D &polyB )
			{
				EFloat	zA	= Max( polyA.transformList[0].z, Max( polyA.transformList[1].z, polyA.transformList[2].z ) );
				EFloat	zB	= Max( polyB.transformList[0].z, Max( polyB.transformList[1].z, polyB.transformList[2].z ) );
				/* 这里写成 < 在debug模式下会崩溃 */
				if ( Abs( zA - zB ) < 0.005f )
				{
					zA	= (polyA.transformList[0].z + polyA.transformList[1].z + polyA.transformList[2].z) / 3.0f;
					zB	= (polyB.transformList[0].z + polyB.transformList[1].z + polyB.transformList[2].z) / 3.0f;
					if ( zA <= zB )
						return(false);
					else
						return(true);
				}else if ( zA < zB )
					return(false);
				else
					return(true);
			}
		};
	}
}
