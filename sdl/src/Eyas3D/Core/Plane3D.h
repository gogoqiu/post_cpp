
#include <stdio.h>

namespace Eyas3D
{
	namespace Core
	{
		class Plane3D
		{
		public:
			Vector4D	point;  /* 面上一点 */
			Vector4D	normal; /* 面的法线 */
			Plane3D()
			{
				
			}
			Plane3D( const Vector4D &p, const Vector4D &nor ) : point( p )
			{
				normal = nor;
				normal.normalize();
			};
			Plane3D & operator = (const Plane3D &right){
				point	= right.point;
				normal	= right.normal;
				return(*this);
			};
		};
	}
}
