
#include <stdio.h>

//typedef std::vector<EPolyon4D>	EPolyon4DArray;
//typedef std::vector<EVector4D>	EVector4DArray;

namespace Eyas3D
{
	namespace Core
	{	
		/* 渲染列表 */
		class RenderList4D
		{
		public:
			int	state;                  /* 物体状态 */
			int	attribute;              /* 物体属性 */
			std::list<PolyonF4D> polyData; /* 具体的多边形数据, 使用双端队列, 删除更有效 */
			typedef std::list<PolyonF4D>::iterator Itr;
		};
	}
}
