#include <stdio.h>

namespace Eyas3D
{
	namespace Core
	{
		class Vertex4D
		{
		public:
			float x, y, z, w;
			/* float nx, ny, nz; */
			float u, v;
			Vertex4D() : x ( 0.0f ), y ( 0.0f ), z ( 0.0f ), w ( 1.0f ),
				/* nx(0.0f), ny(0.0f), nz(0.0f), */
				u ( 0.0f ), v ( 0.0f )
			{
			};
			Vertex4D ( const Vector4D &v ) : x ( v.x ), y ( v.y ), z ( v.z ), w ( v.w ),
				/* nx(0.0f), ny(0.0f), nz(0.0f), */
				u ( 0.0f ), v ( 0.0f )
			{
			};
			Vertex4D ( float xf, float yf, float zf ) : x ( xf ), y ( yf ), z ( zf ), w ( 1 ),
				/* nx(0.0f), ny(0.0f), nz(0.0f), */
				u ( 0.0f ), v ( 0.0f )
			{
			};
			Vertex4D operator * ( const Vector4D &right )
			{
				Vertex4D temp = *this;
				temp.x *= right.x, temp.y *= right.y, temp.z *= right.z;
				return ( temp );
			};
			Vertex4D operator - ( const Vertex4D &right )
			{
				Vertex4D temp = *this;
				temp.x -= right.x, temp.y -= right.y, temp.z -= right.z;
				return ( temp );
			};
			Vertex4D& operator = ( const Vertex4D &right )
			{
				x = right.x;
				y = right.y;
				z = right.z;
				w = right.w;
				/* nx	= right.nx; ny	= right.ny; nz	= right.nz; */
				u = right.u;
				v = right.v;
				return ( *this );
			};
			Vertex4D operator + ( const Vector4D &right )
			{
				Vertex4D temp = *this;
				temp.x += right.x, temp.y += right.y, temp.z += right.z;
				return ( temp );
			};
			Vertex4D operator + ( const Vertex4D &right )
			{
				Vertex4D temp = *this;
				temp.x += right.x, temp.y += right.y, temp.z += right.z;
				return ( temp );
			};
			Vertex4D operator / ( float factor )
			{
				Vertex4D temp = *this;
				temp.x /= factor, temp.y /= factor, temp.z /= factor;
				return ( temp );
			};
			Vector4D toVector4D() const
			{
				return ( Vector4D ( x, y, z, w ) );
			};
		}
	}
}
