#include <stdio.h>

//typedef std::vector<EPolyon4D>	EPolyon4DArray;
//typedef std::vector<EVector4D>	EVector4DArray;
namespace Eyas3D
{
	namespace Core
	{
		class Object4D
		{
		public:
			EString name;                   /* 物体名称 */
			EInt	state;                  /* 物体状态 */
			EInt	attribute;              /* 物体属性 */
			EFloat	avgRadius;              /* 物体的平均半径, 用于碰撞检测 */
			EFloat	maxRadius;              /* 物体的最大半径 */
			EBool needCull;                 /* 是否需要执行剔除操作 */
			EString materiaName;            /* 模型材质的名称--for Ogre Mesh */
			EVector4D	minBoundingBox; /* 模型正方体包围盒最小点 */
			EVector4D	maxBoundingBox; /* 模型正方体包围盒做大点 */
			/* 这里不论使评议还是缩放, 均以第一个模型的参数为准 */
			EVector4D	worldPosition;  /* 物体当前在世界坐标中的位置 */
			EVector4D	scale;          /* 物体当前缩放值 */
			EVector4D direction;            /* 物体的方向向量, 各分量记录延各轴的旋转角度 */
			/* EVector4D		ux, uy, uz;		// 记录物体朝向的局部坐标轴, 物体旋转时将相应的旋转 */
			EInt vertexNumber;              /* 顶点个数 */
			/*
			 * EVector4DArray	localList;		// 物体变换前的物体局部坐标数组
			 * EVector4DArray	transformList;	// 物体变换后的坐标
			 */
			EVertex4DArray	localList;      /* 物体变换前的物体局部坐标数组 */
			EVertex4DArray	transformList;  /* 物体变换后的坐标 */
			EInt		polyonNumber;   /* 物体的所包含多边形的个数 */
			EPolyon4DArray	polyonList;     /* 存储多边形的数组 */
			/*
			 * 这个变量是为了处理一个导入的mesh有多个submesh的情况来考虑的, 如果有多个submesh的话
			 * nextObject != NULL 否则该指针会指向下一个submesh, 直至nextObject == NULL
			 */
			EObject4D *nextObject;
			EObject4D(): nextObject( NULL ), materiaName( DEFAULT_NAME ),
				scale( 1, 1, 1 ), direction( EVector4D::UNIT_X ), needCull( true )
				/*,ux(EVector4D::UNIT_X), uy(EVector4D::UNIT_Y), uz(EVector4D::UNIT_Z)*/ {
			};
		};
	}
}
