
#include <stdio.h>

//typedef std::vector<EPolyon4D>	EPolyon4DArray;
//typedef std::vector<EVector4D>	EVector4DArray;
namespace Eyas3D
{
	namespace Core
	{
		class PolyonF4D
		{
		public:
			int	state;                          /* 状态信息 */
			int	attribute;                      /* 多边形物理属性 */
			int	color;                          /* 多边形光照强度颜色 */
			/*
			 * EVector4D localList[3];			// 物体局部坐标
			 * EVector4D transformList[3];		// 物体局部坐标经变换之后的坐标
			 */
			Vertex4D	localList[3];           /* 物体局部坐标 */
			Vertex4D	transformList[3];       /* 物体局部坐标经变换之后的坐标 */
			/*
			 * EPolyonF4D* pre;	// 链表之前
			 * EPolyonF4D* next;	// 链表之后
			 */
			struct Material *material;
			PolyonF4D(): state( 0 ), attribute( 0 ), color( 0xffffff ) /*,pre(NULL), next(NULL)*/
			{
			};
		};
	}
}
