
#include <stdio.h>

//typedef std::vector<EPolyon4D>	EPolyon4DArray;
//typedef std::vector<EVector4D>	EVector4DArray;
namespace Eyas3D
{
	namespace Core
	{
		class Polyon4D
		{
		public:
			int	state;                  /* 状态信息 */
			int	attribute;              /* 多边形物理属性 */
			int	color;                  /* 多边形颜色 */
			/* EVector4DArray	*verList;	// 顶点列表 vList */
			//EVertex4DArray	*verList;       /* 这里的引用是指的transformList */
			std::vector<EVector4D> *verList; 
			int		verIndex[3];    /* 定点索引 */
			Polyon4D(): state( 0 ), attribute( 0 ), color( 0xffffff ), verList( NULL )
			{
			};
		};
	}
}
