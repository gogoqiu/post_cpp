

#pragma once

namespace Eyas3D
{
	enum System{ Windows, Linux, Macos };
	struct ERuntime
	{		
		System system;
		int memsize;
		char runTimePath[256];
		static ERuntime* runtime;
		ERuntime();
		void info();
		char separator;
	};
}
