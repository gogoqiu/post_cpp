
#pragma once

#include "ECommon.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

using namespace std;
namespace Eyas3D
{
	//extern HINSTANCE GHInstance;
	//SDL_Window* window;
	class EBitmap
	{
		enum Action{ load, create	};
	public:
		//enum BPP{ load, create };
		Action action;
		EInt width, height;
		EBitmap( const EString &filename );
		EBitmap( int width, int height, int bpp );
		~EBitmap();
		bool save( const EString &filename, bool overwrite=false );
		bool setPixel ( int x, int y, Uint32 pixel );
		bool setPixel ( int x, int y, EColor c );
		//log: load, setpixel, save...
		inline EString getName() const
		{
			return(name);
		}
		inline bool isValid() const
		{
			return(valid);
		}
		bool getPixel( EInt x, EInt y, EColor *c );
		bool getPixel( EInt x, EInt y, Uint32 *c );
		void viewer();
		void info();
		inline EInt getHeight() const
		{
			return(height);
		}
		inline EInt getWidth() const
		{
			return(width);
		}
	private:
		EString name;
		SDL_Surface* surface;
		int bpp;
	public:
		EColor *pixels;
		EInt	pitch;
		EBool	valid;		
	};
	
	class EGraphics
	{
	private:
		void plot_circle_points ( int xc, int yc, int x, int y, Uint32 c );
	public:
		static EGraphics *graphic;
		/* 初始化绘图系统 */
		bool initGraphics( SDL_Window* window );
		bool initGraphics();
		/* 关闭绘图系统 */
		void shutdownGraphics();
		/* 检测z值, 返回true则表示通过, 可以调用setPixel */
		EBool checkZ( EInt x, EInt y, EFloat z );
		bool setPixel( EInt x, EInt y, /*EFloat z, */ const EColor &c );
		bool setPixel( EInt x, EInt y, /*EFloat z, */Uint32 pixel );
		//返回值为bool，一旦在处理的过程中异常，就返回失败
		bool getPixel( EInt x, EInt y, EColor *c );
		bool getPixel( EInt x, EInt y, Uint32 *c );
		/* 在当前缓冲区内绘制一条线 */
		void draw_point ( int x, int y, Uint32 color );
		void drawLine( EInt x0, EInt y0, EInt x1, EInt y1, const EColor &c );
		void drawLine ( int x1, int y1, int x2, int y2, Uint32 c );
		void drawCircle ( int xc, int yc, int radius, Uint32 c );
		void drawCircle ( int xc, int yc, int radius, const EColor &c );
		//void plot_circle_points ( int xc, int yc, int x, int y, Uint32 c );
		void drawString( const EString &str, EInt x, EInt y, const EColor &c = EColor( 255, 255, 255 ) );
		void seed_filling ( int i, int j, Uint32 color_fill, Uint32 boundary_color );
		void fillTriangle( EInt x0, EInt y0, EInt x1, EInt y1, EInt x2, EInt y2,
					  const EColor &c = EColor( 255, 255, 255 ) );
		void enableSmoothingMode( EBool enable );
		/* 清空当前缓冲区, 并将其颜色设置为黑色 */
		void clearBuffer( const EColor &c = EColor() );
		/* 将已经绘制好的缓冲区递交给Graphics在屏幕上绘制, 并将当前缓冲区设置为另一个缓冲区 */
		void fillBuffer( SDL_Window* window );
		void fillBuffer();
		EInt getScreenWidth()
		{
			return(SCREEN_WIDTH);
		}
		EInt getScreenHeight()
		{
			return(SCREEN_HEIGHT);
		}
		static void playSound( const char* path ){};
	private:
//sdl
		TTF_Font *font;
		SDL_Window* window;
		//核心对象
		SDL_Surface* surface;
		EFloat *GZBuffer;
	//extended
	
	};
	class ESurface
	{
	private:
		SDL_Surface* surface;
	public:
		ESurface( SDL_Window * window, int w, int h, int ddp );
		ESurface( SDL_Window * window, int w, int h );
		bool InnerSurface_drawPixel( int x, int y, EColor c );
		bool InnerSurface_drawPixel( int x, int y, Uint32 c );
		bool InnerSurface_drawTriangles();
		bool InnerSurface_drawLine( EInt x0, EInt y0, EInt x1, EInt y1, const EColor &c );
		bool InnerSurface_drawLine( int x1, int y1, int x2, int y2, Uint32 c );
		bool InnerSurface_drawString( const EString &str, EInt x, EInt y, const EColor &c );
		bool surfaceUpdate2Window();
	};
	class EScreen:public EGraphics
	{
	};
}
