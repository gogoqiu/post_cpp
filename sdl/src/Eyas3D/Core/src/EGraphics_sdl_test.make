#link
EGraphics_sdl_test: EGraphics_sdl_test.o EGraphics_sdl.o
	cc -o EGraphics_sdl_test EGraphics_sdl_test.o EGraphics_sdl.o -lSDL2  -lstdc++ -lSDL2_ttf
#compile
EGraphics_sdl.o : EGraphics_sdl.cpp
	cc -c EGraphics_sdl.cpp -D_REENTRANT -I/usr/include/SDL2 -std=c++11

EGraphics_sdl_test.o : EGraphics_sdl_test.cpp 
	cc -c EGraphics_sdl_test.cpp  -D_REENTRANT -I/usr/include/SDL2  -std=c++11

clean:
	rm EGraphics_sdl_test EGraphics_sdl_test.o EGraphics_sdl.o
