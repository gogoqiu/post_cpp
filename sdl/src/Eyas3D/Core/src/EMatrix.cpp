
#include "EMatrix.h"

namespace Eyas3D
{
	EMatrix44 EMatrix44::IDENTITY(	1, 0, 0, 0,
									0, 1, 0, 0,
									0, 0, 1, 0,
									0, 0, 0, 1);
}