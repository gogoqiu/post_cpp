
#include "EGraphics.h"
#include <stdio.h>     
 #include <unistd.h>   
 
using namespace Eyas3D;
using namespace std;

void viewWindow( Uint32 *pixels )
{
	
}

int main( int argc, char* argv[] )
{
	//当前搜索目录
	char buf[80];   
	getcwd ( buf, sizeof ( buf ) );
	printf ( "current working directory: %s\n", buf );
	EBitmap white( "white.bmp" );
	EColor c;
	Uint32 color;
	bool retval;
	white.getPixel( 255, 255, &color );
	cout<<color<<endl;
	white.getPixel( 255, 255, &c );
	//cout<<c.r<<"|"<<c.g<<"|"<<c.b<<endl;
	printf( "c.r: %d, c.g: %d, c.b: %d\n", c.r, c.g, c.b );
	white.info();
	white.getPixel( 1, 1, &color );
	cout<<color<<endl;
	//printf( "c.r: %d, c.g: %d, c.b: %d\n", c.r, c.g, c.b );
	EBitmap black( "black.bmp" );
	//EColor c;
	//Uint32 color;
	black.getPixel( 255, 255, &color );
	cout<<color<<endl;
	black.getPixel( 255, 255, &c );
	//cout<<c.r<<"|"<<c.g<<"|"<<c.b<<endl;
	printf( "c.r: %d, c.g: %d, c.b: %d\n", c.r, c.g, c.b );
	black.info();
	black.getPixel( 1, 1, &color );
	cout<<color<<endl;
	if( black.getPixel( 2555, 255, &color ) )
		cout<<color<<endl;
	else
		cout<<"false"<<endl;
	//create a bitmap
	EColor pixel;
	//EBitmap bitmap( 200, 200, 3 );
	EBitmap bitmap( 200, 200, 4 );
	//int index
	for( EUChar x=0; x<200; x++ ){
		for( EUChar y=0; y<200; y++ ){
			//cout<< x<<", "<<y<<endl;
			pixel.from( x, y, 0 );
			bitmap.setPixel( x, y, pixel );
		}
	}
	bitmap.save( "bitmap1.bmp" );
	retval = bitmap.getPixel( 200, 200, &color );
	cout<<color<<endl;
	retval = bitmap.getPixel( 199, 199, &color );
	cout<<color<<endl;
	cout<<"color:"<<endl;
	c.from( color );
	c.info();
	retval = bitmap.getPixel( 0, 0, &color );
	cout<<color<<endl;
	cout<<"color:"<<endl;
	c.from( color );
	c.info();
	retval = bitmap.getPixel( 0, 5, &color );
	cout<<color<<endl;
	cout<<"color:"<<endl;
	c.from( color );
	c.info();
	bitmap.info();
}
