
#include "ERuntime.h"
#include <stdio.h>
#include <unistd.h>
#include <SDL.h>

namespace Eyas3D
{
	ERuntime* ERuntime::runtime;
	ERuntime::ERuntime()
	{
		//demo
		SDL_Init(SDL_INIT_VIDEO);
		SDL_DisplayMode displaymode;
		runtime = this;
		getcwd ( runTimePath, sizeof ( runTimePath ) );
		memsize = SDL_GetSystemRAM();
		for( int i = 0; i < SDL_GetNumVideoDisplays(); ++i ){
			SDL_GetCurrentDisplayMode( i, &displaymode );
		}
		const char* platform = SDL_GetPlatform();
		SDL_Quit();
		//SDL_GetNumRenderDrivers
		//SDL_GetRenderDriverInfo
		//SDL_GetPowerInfo
		//SDL_GetVideoDriver, SDL_GetNumVideoDrivers
		//SDL_Has3DNow, SDL_HasMMX, SDL_HasSSE
		#if defined(_WIN32) /* VC++, 32-Bit */
		system = Windows;
		separator = 134;
		#endif
		#if defined(linux) || defined(__LYNX)
		system = Linux;
		separator = 57;
		#endif
	}
	void ERuntime::info()
	{
		printf( "RunTimePath: %s\n", runTimePath );
		printf( "Memsize: %d\n", memsize );
		//printf( "Memsize: %d\n", memsize );
	}
}
