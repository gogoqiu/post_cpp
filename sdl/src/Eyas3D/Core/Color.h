


#define ECOLOR_16BIT(r, g, b) (((r) & 0xff) << 16 | ((g) & 0xff) << 8 | (b) & 0xff)
//	(b) & 0xff, 保留b的最右8位
//
#include <stdio.h>
namespace Eyas3D
{
	namespace Core
	{
		class Color
		{
		private:
			//unsigned char r , g, b, a;
		public:
			unsigned char r , g, b, a;
			Color( int color, unsigned char alpha = 255) 
			{ 
				r = (unsigned char)((color & 0xff0000) >> 16);
				g = (unsigned char)((color & 0x00ff00) >> 8);
				b = (unsigned char)(color & 0x0000ff);
				a = alpha;
			}
			Color( unsigned char ri = 0, unsigned char gi = 0, unsigned char bi = 0, unsigned char ai = 255) : r(ri), g(gi), b(bi), a(ai){}
			int ToInt() const { return ECOLOR_16BIT(r, g, b); }
			Color operator *(const Color &c) const
			{
				int ri = r * c.r >> 8;	// r * c.r / 256
				int gi = g * c.g >> 8;
				int bi = b * c.b >> 8;
				int ai = a * c.a >> 8;
				return Color(ri, gi, bi, ai);
			}
			Color operator =( const Color &c ) const
			{
				//this->r = c.r;	
				//r  = 
				//this->g = c.g;	
				//this->b=c.b;	this->a=c.a;
			}
			static void fill( int color, Color *c ){
				c->r = (unsigned char)((color & 0xff0000) >> 16);
				c->g = (unsigned char)((color & 0x00ff00) >> 8);
				c->b = (unsigned char)(color & 0x0000ff);
				c->a = 255;
			}
			void from( unsigned int color ){
				r = (unsigned char)((color & 0xff0000) >> 16);
				g = (unsigned char)((color & 0x00ff00) >> 8);
				b = (unsigned char)(color & 0x0000ff);
				a = 255;
			}
			void from( unsigned char r, unsigned char g, unsigned char b ){
				this->r = r;	this->g = g;	this->b = b;	this->a = 255;
			}
			void info(){
				printf( "%d, %d, %d, %d\n", r, g, b, a );
			}
			static void demo1(){
				//create color
				Color c1( 255, 255, 255 );
				c1.info();
			}
			//demo2
			static void demo2(){
				
			}
		};
	}
}
