
#pragma once

namespace Eyas3D
{
	/* 鼠标键盘监听器 */
	class EInputListener
	{
	public:
		virtual EBool keyPress( EInt key ) = 0;
		virtual EBool keyRlease( EInt key ) = 0;
		virtual EBool mouseButtonDown( EInt mouseButton ) = 0;
		virtual EBool mouseButtonRelease( EInt mouseButton ) = 0;
		virtual EBool mouseMove( EInt x, EInt y ) = 0;
		virtual EBool mouseWheel( EInt delta ) = 0;
		virtual ~EInputListener()
		{
		}
	};
}
