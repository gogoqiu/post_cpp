

using namespace std;
namespace Eyas3D
{
	/*SDL_Surface* SDL_CreateRGBSurface(Uint32 flags,
                                  int    width,
                                  int    height,
                                  int    depth,
                                  Uint32 Rmask,
                                  Uint32 Gmask,
                                  Uint32 Bmask,
                                  Uint32 Amask)
	SDL_Surface* SDL_CreateRGBSurfaceFrom(void*  pixels,
                                      int    width,
                                      int    height,
                                      int    depth,
                                      int    pitch,
                                      Uint32 Rmask,
                                      Uint32 Gmask,
                                      Uint32 Bmask,
                                      Uint32 Amask)
	SDL_Surface* SDL_LoadBMP(const char* file)
	*/
	//type 1
	class EBitmap
	{
	private:
		EString name;
		SDL_Surface* surface;
		//EColor *pixels;
		UINT *pixels;
		EInt	pitch;
		EBool	valid;
	public:
		EInt width, height;
		EBitmap( const EString &filename ){
			surface = SDL_LoadBMP( filename.c_str() );
			//如果不是一个整数一个像素,就否决
			if( surface!=NULL )
				valid= true;
		}
		~EBitmap();
		inline EString getName() const
		{
			return(name);
		}
		inline bool isValid() const
		{
			return(valid);
		}
		/* height times pitch is the size of the surface's whole buffer. */
		EColor getPixel( EInt x, EInt y )
		{
			//支持什么类型的图片
			SDL_PixelFormat *fmt;
			switch( surface->format->palette ){
			case SDL_PIXELFORMAT_RGB888:
				break;
			case SDL_PIXELFORMAT_RGBX8888:
				break;
			default :
				//unknown format
				break;
			}
			/* Lock the surface */
			SDL_LockSurface(surface);
			/* Get the topleft pixel */
			//index=*(Uint8 *)surface->pixels;
			//color=&fmt->palette->colors[index];
			/* Unlock the surface */
			Uint pixel = (Uint)surface->pixels[pitch*y+x];
			SDL_UnlockSurface(surface);
			return EColor( pixel );
		}
		inline EInt getHeight() const
		{
			if( surface!=NULL )
				return(surface->h);
		}
		inline EInt getWidth() const
		{
			if( surface!=NULL )
				return(surface->w);
		}
		inline EInt getPitch() const
		{
			if( surface!=NULL )
				return surface->pitch;
		}
	}
	//
	//type 2
	//
}
