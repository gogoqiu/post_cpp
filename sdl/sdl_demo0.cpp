
#include <SDL.h>
//#include <SDL_video.h>
#include <stdio.h>
#include <unistd.h>

using namespace Eyas3D;
using namespace std;
//window
//surface
//texture
//renderer info
//int SDL_GetNumRenderDrivers(void)
//int SDL_GetRendererInfo(SDL_Renderer*     renderer, SDL_RendererInfo* info)
//SDL_Renderer* SDL_CreateRenderer(SDL_Window* window,            int         index,                              Uint32      flags)

void viewWindow ( Uint32 *pixels )
{

}

/*
 * struct SDL_Surface
 * {
 * 	SDL_PixelFormat* format //the format of the pixels stored in the surface; see SDL_PixelFormat for details (read-only)
		int w, h//the width and height in pixels (read-only)
		int pitch//the length of a row of pixels in bytes (read-only)
		void* pixels//the pointer to the actual pixel data; see Remarks for details (read-write)
		void* userdata//an arbitrary pointer you can set (read-write)
		int locked//used for surfaces that require locking (internal use)
		void* lock_data//used for surfaces that require locking (internal use)
		SDL_Rect clip_rect//an SDL_Rect structure used to clip blits to the surface which can be set by SDL_SetClipRect() (read-only)
		SDL_BlitMap* map//info for fast blit mapping to other surfaces (internal use)
		int refcount;//reference count that can be incremented by the application
 * }
 * struct SDL_Texture
 * {
 * 	...
 * }
 */
/* This is meant to show how to edit a surface's pixels on the CPU, but
   normally you should use SDL_FillRect() to wipe a surface's contents. */
void WipeSurface(SDL_Surface *surface)
{
    /* This is fast for surfaces that don't require locking. */
    /* Once locked, surface->pixels is safe to access. */
    SDL_LockSurface(surface);
    /* This assumes that color value zero is black. Use
       SDL_MapRGBA() for more robust surface color mapping! */
    /* height times pitch is the size of the surface's whole buffer. */
    SDL_memset(surface->pixels, 0, surface->h * surface->pitch);
    SDL_UnlockSurface(surface);
}

SDL_Texture* setupTexture ( SDL_Renderer* renderer )
{
	//定制texture
	SDL_Texture * texture = SDL_CreateTexture ( renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 1024, 768 );
	SDL_SetRenderTarget ( renderer, texture );
	SDL_SetRenderDrawColor ( renderer, 0x00, 0x00, 0x00, 0x00 );
	SDL_RenderClear ( renderer );
	return texture;
}

int main ( int argc, char* argv[] )
{
	//当前搜索目录
	char buf[80];
	getcwd ( buf, sizeof ( buf ) );
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Surface *surface;
	SDL_Texture *texture;
	SDL_Event event;
	if ( SDL_Init ( SDL_INIT_VIDEO ) < 0 ) {
		SDL_LogError ( SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s", SDL_GetError() );
		return 3;
	}
	if ( SDL_CreateWindowAndRenderer ( 320, 240, SDL_WINDOW_RESIZABLE, &window, &renderer ) ) {
		SDL_LogError ( SDL_LOG_CATEGORY_APPLICATION, "Couldn't create window and renderer: %s", SDL_GetError() );
		return 3;
	}
	surface = SDL_LoadBMP ( "sample.bmp" );
	if ( !surface ) {
		SDL_LogError ( SDL_LOG_CATEGORY_APPLICATION, "Couldn't create surface from image: %s", SDL_GetError() );
		return 3;
	}
	texture = SDL_CreateTextureFromSurface ( renderer, surface );
	if ( !texture ) {
		SDL_LogError ( SDL_LOG_CATEGORY_APPLICATION, "Couldn't create texture from surface: %s", SDL_GetError() );
		return 3;
	}
	SDL_FreeSurface ( surface );
	while ( 1 ) {
		SDL_PollEvent ( &event );
		if ( event.type == SDL_QUIT ) {
			break;
		}
		SDL_SetRenderDrawColor ( renderer, 0x00, 0x00, 0x00, 0x00 );
		SDL_RenderClear ( renderer );
		SDL_RenderCopy ( renderer, texture, NULL, NULL );
		SDL_RenderPresent ( renderer );
	}
	SDL_DestroyTexture ( texture );
	SDL_DestroyRenderer ( renderer );
	SDL_DestroyWindow ( window );
	SDL_Quit();
	return 0;
}
